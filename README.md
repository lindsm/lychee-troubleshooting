# Lychee Troubleshooting

Trying to print MGpix's [Dungeon Lab Modular Tile System](https://www.myminifactory.com/object/3d-print-dungeon-lab-238300) Walls on a Elegoo Jupiter, Anycubic Photon Ultra, and Elegoo Saturn 2 using files exported from Lychee.  The prints fail.  Printing the same files through Chitubox succeeds.

## Story So Far

Opened the files with Lychee v 3.19(FIXME).  Errors were reported.  Ran repair on them, errors remained.  Reached out to MGpix on discord, he said they should still print and provided multiple examples of other users printing them.  Printed the walls out and their were problems.  The same problems existed on all the printers when the exported Lychee files were used.  The printed objects look like they have geometry going through them.  Take the same files and open them up in Chitubox and export, everything looks as expected.

In the following image, notice the really large "bars" and ... stryations in the rock wall face.  Looks like the geometry was pulled or invertered from the object, and shows on the outside instead.

![Broken High Wall](img/failed_high_wall.jpg)

Here is a successful print using Chitubox.  No bars or stryation, and the objects look complete.

![Successful High Wall](img/successful_high_wall.jpg)

## Setup

**Lychee Version**: 4.1
**Resins**: Siraya Tech Grey, Siraya Tech Blu, Elegoo Grey
